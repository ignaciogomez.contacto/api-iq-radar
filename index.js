const express = require('express')
const app = express()
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const cors = require('cors')
require('./controllers/crom_data.js')
const data = require('./routes/data')
dotenv.config()

var allowedOrigins = [
  'http://localhost:3000',
  'https://iq-radar.netlify.app',
  'https://www.contratopublico.es',
]

app.use(
  cors({
    origin: function (origin, callback) {
      if (!origin) return callback(null, true)
      if (allowedOrigins.indexOf(origin) === -1) {
        var msg =
          'The CORS policy for this site does not ' +
          'allow access from the specified Origin.'
        return callback(new Error(msg), false)
      }
      return callback(null, true)
    },
  })
)
app.use(data)
const DB = process.env.DB || 'mongodb://localhost/concurso-publico'
mongoose.connect(DB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
})
console.log('aaa')
const port = 5000
app.listen(port)
