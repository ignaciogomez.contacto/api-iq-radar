const CronJob = require('cron').CronJob
const axios = require('axios')
const parser = require('fast-xml-parser')
const he = require('he')
const mongoose = require('mongoose')

const ObjectsToCsv = require('objects-to-csv')
const Data = require('../models/data')
const CodeType = require('../models/code_type')
const xmlToObject = async (xml) => {
  try {
    const options = {
      attributeNamePrefix: '@_',
      attrNodeName: 'attr', //default is 'false'
      textNodeName: '#text',
      ignoreAttributes: true,
      ignoreNameSpace: true,
      allowBooleanAttributes: true,
      parseNodeValue: true,
      parseAttributeValue: true,
      trimValues: true,
      cdataTagName: '__cdata', //default is 'false'
      cdataPositionChar: '\\c',
      parseTrueNumberOnly: true,
      arrayMode: false, //"strict"
      attrValueProcessor: (val, attrName) =>
        he.decode(val, { isAttributeValue: true }), //default is a=>a
      tagValueProcessor: (val, tagName) => he.decode(val), //default is a=>a
      stopNodes: ['parse-me-as-string'],
    }

    if (parser.validate(xml) === true) {
      var jsonObj = parser.parse(xml, options)
      var result = []
      const entries = jsonObj.feed.entry
      for (let object of entries) {
        result.push(flatten(object))
        var itemsItemClassificationCode = []

        if (
          Array.isArray(
            object.ContractFolderStatus?.ProcurementProject
              ?.RequiredCommodityClassification?.ItemClassificationCode
          )
        ) {
          for (let item of object.ContractFolderStatus?.ProcurementProject
            ?.RequiredCommodityClassification?.ItemClassificationCode) {
            const codeType = await CodeType.findOne({ code: item.code })
            if (codeType) {
              itemsItemClassificationCode.push(codeType._id)
            }
          }
        } else {
          const codeType = await CodeType.findOne({
            code: object.ContractFolderStatus?.ProcurementProject
              ?.RequiredCommodityClassification?.ItemClassificationCode,
          })
          if (codeType) {
            itemsItemClassificationCode.push(codeType._id)
          }
        }

        const entry = {
          id: object.id,
          title: object.title,
          updated: object.updated,

          cfs_contractFolderID: object.ContractFolderStatus.ContractFolderID,
          cfs_contractFolderStatusCode:
            object.ContractFolderStatus.ContractFolderStatusCode,

          cfs_lcp_ContractingPartyTypeCode:
            object.ContractFolderStatus.LocatedContractingParty
              .ContractFolderStatusCode,
          cfs_lcp_p_websiteURI:
            object.ContractFolderStatus.LocatedContractingParty.Party
              .WebsiteURI,
          cfs_lcp_p_partyIdentification:
            object.ContractFolderStatus.LocatedContractingParty.Party
              .PartyIdentification,
          cfs_lcp_p_pn_name:
            object.ContractFolderStatus.LocatedContractingParty.Party.PartyName
              .Name,
          cfs_lcp_p_pa_cityName:
            object.ContractFolderStatus.LocatedContractingParty.Party
              .PostalAddress.CityName,
          cfs_lcp_p_pa_postalZone:
            object.ContractFolderStatus.LocatedContractingParty.Party
              .PostalAddress.PostalZone,
          cfs_lcp_p_pa_al_line:
            object.ContractFolderStatus.LocatedContractingParty.Party
              .PostalAddress.AddressLine.Line,
          cfs_lcp_p_pa_c_identificationCode:
            object.ContractFolderStatus.LocatedContractingParty.Party
              .PostalAddress.Country.IdentificationCode,
          cfs_lcp_p_pa_c_name:
            object.ContractFolderStatus.LocatedContractingParty.Party
              .PostalAddress.Country.Name,
          cfs_lcp_p_c_name:
            object.ContractFolderStatus.LocatedContractingParty.Party.Contact
              .Name,
          cfs_lcp_p_c_telephone:
            object.ContractFolderStatus.LocatedContractingParty.Party.Contact
              .Telephone,
          cfs_lcp_p_c_telefax:
            object.ContractFolderStatus.LocatedContractingParty.Party.Contact
              .Telefax,
          cfs_lcp_p_c_electronicMail:
            object.ContractFolderStatus.LocatedContractingParty.Party.Contact
              .ElectronicMail,

          cfs_pp_name: object.ContractFolderStatus.ProcurementProject.Name,
          cfs_pp_typeCode:
            object.ContractFolderStatus.ProcurementProject.TypeCode,
          cfs_pp_SubTypeCode:
            object.ContractFolderStatus?.ProcurementProject.SubTypeCode,
          cfs_pp_ba_estimatedOverallContractAmount:
            object.ContractFolderStatus.ProcurementProject.BudgetAmount
              .EstimatedOverallContractAmount,
          cfs_pp_ba_totalAmount:
            object.ContractFolderStatus.ProcurementProject.BudgetAmount
              .TotalAmount,
          cfs_pp_ba_taxExclusiveAmount:
            object.ContractFolderStatus.ProcurementProject.BudgetAmount
              ?.TaxExclusiveAmount,
          cfs_pp_rcc_itemClassificationCode: itemsItemClassificationCode,
          cfs_pp_rl_countrySubentity:
            object.ContractFolderStatus.ProcurementProject.RealizedLocation
              .CountrySubentity,
          cfs_pp_rl_countrySubentityCode:
            object.ContractFolderStatus.ProcurementProject.RealizedLocation
              .CountrySubentityCode,
          cfs_pp_rl_a_c_identificationCode:
            object.ContractFolderStatus.ProcurementProject.RealizedLocation
              .Address?.Country.IdentificationCode,
          cfs_pp_rl_a_c_name:
            object.ContractFolderStatus.ProcurementProject.RealizedLocation
              .Address?.Country.Name,
          cfs_pp_pp_durationMeasure:
            object.ContractFolderStatus.ProcurementProject.PlannedPeriod
              ?.DurationMeasure,

          cfs_tr_resultCode:
            object.ContractFolderStatus.TenderResult?.ResultCode,
          cfs_tr_description:
            object.ContractFolderStatus.TenderResult?.Description,
          cfs_tr_awardDate: object.ContractFolderStatus.TenderResult?.AwardDate,
          cfs_tr_receivedTenderQuantity:
            object.ContractFolderStatus.TenderResult?.ReceivedTenderQuantity,
          cfs_tr_lowerTenderAmount:
            object.ContractFolderStatus.TenderResult?.LowerTenderAmount,
          cfs_tr_higherTenderAmount:
            object.ContractFolderStatus.TenderResult?.HigherTenderAmount,
          cfs_tr_wp_pi_iD:
            object.ContractFolderStatus.TenderResult?.WinningParty
              ?.PartyIdentification.ID,
          cfs_tr_wp_pn_Name:
            object.ContractFolderStatus.TenderResult?.WinningParty?.PartyName
              .Name,
          cfs_tr_atp_lmt_taxExclusiveAmount:
            object.ContractFolderStatus.TenderResult?.AwardedTenderedProject
              ?.LegalMonetaryTotal?.TaxExclusiveAmount,
          cfs_tr_atp_lmt_payableAmount:
            object.ContractFolderStatus.TenderResult?.AwardedTenderedProject
              ?.LegalMonetaryTotal?.PayableAmount,

          cfs_tt_requiredCurriculaIndicator:
            object.ContractFolderStatus.TenderingTerms
              .RequiredCurriculaIndicator,
          cfs_tt_VariantConstraintIndicator:
            object.ContractFolderStatus.TenderingTerms
              .VariantConstraintIndicator,
          cfs_tt_pldr_iD:
            object.ContractFolderStatus.TenderingTerms
              .ProcurementLegislationDocumentReference?.ID,
          cfs_tt_tqr_technicalEvaluationCriteria:
            object.ContractFolderStatus.TenderingTerms
              .TendererQualificationRequest?.TechnicalEvaluationCriteria,
          cfs_tt_tqr_financialEvaluationCriteria:
            object.ContractFolderStatus.TenderingTerms
              .TendererQualificationRequest?.FinancialEvaluationCriteria,
          cfs_tt_tqr_str_requirementTypeCode:
            object.ContractFolderStatus.TenderingTerms
              .TendererQualificationRequest?.SpecificTendererRequirement
              ?.RequirementTypeCode,
          cfs_tt_ast_Rate:
            object.ContractFolderStatus?.TenderingTerms?.AllowedSubcontractTerms
              ?.Rate,

          cfs_tt_tqr_at_awardingCriteria:
            object.ContractFolderStatus.TenderingTerms.AwardingTerms
              ?.AwardingCriteria,
          cfs_tt_tqr_at_l_iD:
            object.ContractFolderStatus.TenderingTerms.Language.ID,

          cfs_tp_procedureCode:
            object.ContractFolderStatus.TenderingProcess.ProcedureCode,
          cfs_tp_urgencyCode:
            object.ContractFolderStatus.TenderingProcess.UrgencyCode,
          cfs_tp_contractingSystemCode:
            object.ContractFolderStatus.TenderingProcess.ContractingSystemCode,
          cfs_tp_submissionMethodCode:
            object.ContractFolderStatus.TenderingProcess.SubmissionMethodCode,
          cfs_tp_tsdp_endDate:
            object.ContractFolderStatus?.TenderingProcess
              ?.DocumentAvailabilityPeriod?.EndDate,
          cfs_tp_tsdp_endTime:
            object.ContractFolderStatus?.TenderingProcess
              ?.DocumentAvailabilityPeriod?.EndTime,
          cfs_tp_tsdp_endDatetime:
            new Date(
              `${
                object.ContractFolderStatus?.TenderingProcess
                  ?.DocumentAvailabilityPeriod?.EndDate || '1-1-70'
              }Z${
                object.ContractFolderStatus?.TenderingProcess
                  ?.DocumentAvailabilityPeriod?.EndTime || '00:00'
              }`
            ) || null,
          cfs_tp_at_AuctionConstraintIndicator:
            object.ContractFolderStatus?.TenderingProcess?.AuctionTerms
              ?.AuctionConstraintIndicator,
          cfs_ldr_iD: object.ContractFolderStatus.LegalDocumentReference?.ID,
          cfs_ldr_a_er_uri:
            object.ContractFolderStatus.LegalDocumentReference?.Attachment
              .ExternalReference.URI,
          cfs_ldr_a_er_documentHash:
            object.ContractFolderStatus.LegalDocumentReference?.Attachment
              .ExternalReference.DocumentHash,
          cfs_tdr_iD:
            object.ContractFolderStatus.TechnicalDocumentReference?.ID,
          cfs_tdr_a_er_uri:
            object.ContractFolderStatus.TechnicalDocumentReference?.Attachment
              .ExternalReference.URI,
          cfs_tdr_a_er_documentHash:
            object.ContractFolderStatus.TechnicalDocumentReference?.Attachment
              .ExternalReference.DocumentHash,

          cfs_validNoticeInfo: object.ContractFolderStatus.ValidNoticeInfo,

          codeprovince:
            object.ContractFolderStatus?.ProcurementProject?.RealizedLocation?.CountrySubentityCode?.substring(
              0,
              4
            ) || '',

          cfs_GeneralDocument: object.ContractFolderStatus.GeneralDocument,
          // cfs_gd_gddr_iD: { type: String },
          // cfs_gd_gddr_a_er_uri: { type: String },
          // cfs_gd_gddr_a_er_fileName: { type: String },
        }

        // const data = new Data({ result: object })
        //  await data.save()

        const exist = await Data.findOne({
          id: object.id,
        })

        if (exist) {
          await Data.findByIdAndUpdate(
            {
              _id: exist._id,
            },
            entry,
            { upsert: true }
          )
        } else {
          const data = new Data(entry)
          await data.save()
        }
      }
      try {
        await mongoose.connection.db.dropCollection('MapReduceName')
        await mongoose.connection.db.dropCollection('MapReduceName2')
        await mongoose.connection.db.dropCollection('MapReduceProvinces')
        await mongoose.connection.db.dropCollection(
          'MapReduceClassificationCode'
        )
      } catch (e) {}

      var o = {}
      o.map = function () {
        emit(this.cfs_tr_wp_pi_iD, {
          cfs_tr_atp_lmt_payableAmount: this.cfs_tr_atp_lmt_payableAmount,
          cfs_tr_atp_lmt_taxExclusiveAmount:
            this.cfs_tr_atp_lmt_taxExclusiveAmount,
          cfs_tr_wp_pn_Name: this.cfs_tr_wp_pn_Name,
          // list other fields like above to select them
        })
      }
      o.reduce = function (keys, vals) {
        var reducedVals = {
          cfs_tr_atp_lmt_payableAmount: 0,
          cfs_tr_atp_lmt_taxExclusiveAmount: 0,
        }

        for (var i = 0; i < vals.length; i++) {
          reducedVals.cfs_tr_atp_lmt_payableAmount +=
            vals[i].cfs_tr_atp_lmt_payableAmount
          reducedVals.cfs_tr_atp_lmt_taxExclusiveAmount +=
            vals[i].cfs_tr_atp_lmt_taxExclusiveAmount
        }
        reducedVals.cfs_tr_wp_pn_Name = vals[0].cfs_tr_wp_pn_Name
        return reducedVals
      }
      o.out = {
        replace: 'MapReduceName',
      }
      o.verbose = true
      await Data.mapReduce(o)

      var o2 = {}
      o2.map = function () {
        if (this.cfs_lcp_p_partyIdentification.length > 1) {
          emit(this.cfs_lcp_p_partyIdentification[1].ID, {
            cfs_tr_atp_lmt_payableAmount: this.cfs_tr_atp_lmt_payableAmount,
            cfs_tr_atp_lmt_taxExclusiveAmount:
              this.cfs_tr_atp_lmt_taxExclusiveAmount,
            cfs_lcp_p_c_name: this.cfs_lcp_p_c_name,
            // list other fields like above to select them
          })
        }
      }
      o2.reduce = function (keys, vals) {
        var reducedVals = {
          cfs_tr_atp_lmt_payableAmount: 0,
          cfs_tr_atp_lmt_taxExclusiveAmount: 0,
        }

        for (var i = 0; i < vals.length; i++) {
          reducedVals.cfs_tr_atp_lmt_payableAmount +=
            vals[i].cfs_tr_atp_lmt_payableAmount
          reducedVals.cfs_tr_atp_lmt_taxExclusiveAmount +=
            vals[i].cfs_tr_atp_lmt_taxExclusiveAmount
        }
        reducedVals.cfs_lcp_p_c_name = vals[0].cfs_lcp_p_c_name

        return reducedVals
      }
      o2.out = {
        replace: 'MapReduceName2',
      }
      o2.verbose = true
      await Data.mapReduce(o2)

      var objectProvinces = {}
      objectProvinces.map = function () {
        emit(this.codeprovince, {
          cfs_tr_atp_lmt_payableAmount: this.cfs_tr_atp_lmt_payableAmount,
          cfs_tr_atp_lmt_taxExclusiveAmount:
            this.cfs_tr_atp_lmt_taxExclusiveAmount,
          cfs_pp_rl_countrySubentity: this.cfs_pp_rl_countrySubentity,
          // list other fields like above to select them
        })
      }
      objectProvinces.reduce = function (keys, vals) {
        var reducedVals = {
          cfs_tr_atp_lmt_payableAmount: 0,
          cfs_tr_atp_lmt_taxExclusiveAmount: 0,
        }

        for (var i = 0; i < vals.length; i++) {
          reducedVals.cfs_tr_atp_lmt_payableAmount +=
            vals[i].cfs_tr_atp_lmt_payableAmount || 0
          reducedVals.cfs_tr_atp_lmt_taxExclusiveAmount +=
            vals[i].cfs_tr_atp_lmt_taxExclusiveAmount || 0
          reducedVals.cfs_pp_rl_countrySubentity =
            vals[0].cfs_pp_rl_countrySubentity
        }
        return reducedVals
      }

      objectProvinces.out = {
        replace: 'MapReduceProvinces',
      }
      await Data.mapReduce(objectProvinces)

      ///MAP REDUCE ClassificationCode
      var objectClassificationCode = {}
      const objectsCode = (objectClassificationCode.map = function () {
        if (
          this.cfs_pp_rcc_itemClassificationCode &&
          this.cfs_pp_rcc_itemClassificationCode.length > 0
        ) {
          for (
            var i = 0;
            i < this.cfs_pp_rcc_itemClassificationCode.length;
            i++
          ) {
            emit(this.cfs_pp_rcc_itemClassificationCode[i], {
              cfs_tr_atp_lmt_payableAmount: this.cfs_tr_atp_lmt_payableAmount,
              cfs_tr_atp_lmt_taxExclusiveAmount:
                this.cfs_tr_atp_lmt_taxExclusiveAmount,
              cfs_pp_rl_countrySubentity: this.cfs_pp_rl_countrySubentity,
              cfs_pp_rcc_itemClassificationCode:
                this.cfs_pp_rcc_itemClassificationCode[i],
              // list other fields like above to select them
            })
          }
        }
      })
      objectClassificationCode.reduce = function (keys, vals) {
        var reducedVals = {
          cfs_tr_atp_lmt_payableAmount: 0,
          cfs_tr_atp_lmt_taxExclusiveAmount: 0,
        }

        for (var i = 0; i < vals.length; i++) {
          reducedVals.cfs_tr_atp_lmt_payableAmount +=
            vals[i].cfs_tr_atp_lmt_payableAmount || 0
          reducedVals.cfs_tr_atp_lmt_taxExclusiveAmount +=
            vals[i].cfs_tr_atp_lmt_taxExclusiveAmount || 0
        }

        reducedVals.cfs_pp_rl_countrySubentity =
          vals[0].cfs_pp_rl_countrySubentity
        reducedVals.cfs_pp_rcc_itemClassificationCode =
          vals[0].cfs_pp_rcc_itemClassificationCode

        return reducedVals
      }
      objectClassificationCode.out = {
        replace: 'MapReduceClassificationCode',
      }

      objectClassificationCode.verbose = true

      console.log('bbbb')
      await Data.mapReduce(objectClassificationCode)
      ///fin MAP REDUCE ClassificationCode

      // const csv = new ObjectsToCsv(result)
      // await csv.toDisk(`./test-${new Date()}.csv`)
    }
  } catch (e) {
    console.log(e)
  }
}

async function traverseAndFlatten(currentNode, target, flattenedKey) {
  for (var key in currentNode) {
    if (currentNode.hasOwnProperty(key)) {
      var newKey
      if (flattenedKey === undefined) {
        newKey = key
      } else {
        newKey = flattenedKey.trim() + '_' + key.trim()
      }

      var value = currentNode[key]
      if (typeof value === 'object') {
        traverseAndFlatten(value, target, newKey)
      } else {
        target[newKey] = value
      }
    }
  }
}

function flatten(obj) {
  var flattenedObject = {}
  traverseAndFlatten(obj, flattenedObject)
  return flattenedObject
}

var job = new CronJob(
  '0 6,18 * * *',
  //'0,30 * * * * *',
  async function () {
    console.log('crom data')
    try {
      console.log('aa')
      const response = await axios.get(
        'https://contrataciondelestado.es/sindicacion/sindicacion_643/licitacionesPerfilesContratanteCompleto3.atom'
      )
      xmlToObject(response.data)
    } catch (error) {
      console.error(error)
    }
  },
  null,
  true,
  'Atlantic/Canary'
)
job.start()

async function saveObject() {
  try {
    const response = await axios.get(
      'https://contrataciondelestado.es/codice/cl/2.04/CPV2008-2.04.gc'
    )

    const options = {
      attributeNamePrefix: '@_',
      attrNodeName: 'attr', //default is 'false'
      textNodeName: '#text',
      ignoreAttributes: true,
      ignoreNameSpace: true,
      allowBooleanAttributes: true,
      parseNodeValue: true,
      parseAttributeValue: true,
      trimValues: true,
      cdataTagName: '__cdata', //default is 'false'
      cdataPositionChar: '\\c',
      parseTrueNumberOnly: true,
      arrayMode: true, //"strict"
      attrValueProcessor: (val, attrName) =>
        he.decode(val, { isAttributeValue: true }), //default is a=>a
      tagValueProcessor: (val, tagName) => he.decode(val), //default is a=>a
      stopNodes: ['parse-me-as-string'],
    }

    if (parser.validate(response.data) === true) {
      var jsonObj = parser.parse(response.data, options)
      for (const item of jsonObj.CodeList[0].SimpleCodeList[0].Row) {
        var object = {
          code: item.Value[0].SimpleValue,
          type: item.Value[1].SimpleValue,
          type_en: item.Value[2]?.SimpleValue,
        }
        const exist = await CodeType.findOne({ code: object.code })
        if (exist) {
          await CodeType.findByIdAndUpdate(
            {
              _id: exist._id,
            },
            object,
            { upsert: true }
          )
        } else {
          const codeType = new CodeType(object)
          await codeType.save()
        }
      }

      // for (let object of entries) {
      //   result.push(flatten(object))
      //   console.log(object)
      // }
    }
  } catch (error) {
    console.error(error)
  }
}

//saveObject()
