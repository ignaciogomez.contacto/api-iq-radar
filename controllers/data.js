var Data = require('../models/data')
const mongoose = require('mongoose')
const CodeData = require('../models/code_type')

exports.getData = async (req, res, next) => {
  try {
    const pre = await Data.find({
      cfs_contractFolderStatusCode: 'PRE',
    }).countDocuments()
    const pub = await Data.find({
      cfs_contractFolderStatusCode: 'PUB',
    }).countDocuments()
    const ev = await Data.find({
      cfs_contractFolderStatusCode: 'EV',
    }).countDocuments()
    const adj = await Data.find({
      cfs_contractFolderStatusCode: 'ADJ',
    }).countDocuments()
    const resu = await Data.find({
      cfs_contractFolderStatusCode: 'RES',
    }).countDocuments()
    const anul = await Data.find({
      cfs_contractFolderStatusCode: 'ANUL',
    }).countDocuments()

    const connection = mongoose.connection

    const collection = await connection.db.collection('MapReduceName')
    const topTenObject = await collection
      .find()
      .sort({ 'value.cfs_tr_atp_lmt_payableAmount': -1 })
      .limit(10)
      .toArray()
    let topTen = { x: [], y: [] }
    topTenObject.forEach((item) => {
      topTen.y.push(item.value.cfs_tr_wp_pn_Name)
      topTen.x.push(Math.round(item.value.cfs_tr_atp_lmt_payableAmount))
    })
    const collection2 = await connection.db.collection('MapReduceName2')
    const topTenObject2 = await collection2
      .find()
      .sort({ 'value.cfs_tr_atp_lmt_taxExclusiveAmount': -1 })
      .limit(10)
      .toArray()
    let topTen2 = { x: [], y: [] }
    topTenObject2.forEach((item) => {
      topTen2.y.push(item.value.cfs_lcp_p_c_name)
      topTen2.x.push(Math.round(item.value.cfs_tr_atp_lmt_taxExclusiveAmount))
    })
    const collectionMap = await connection.db.collection('MapReduceProvinces')

    const mapsResult = await collectionMap.find().toArray()

    const collectionClassificationCode = await connection.db.collection(
      'MapReduceClassificationCode'
    )
    let topTen3 = []
    const codeResultCodeData = await collectionClassificationCode
      .aggregate([
        {
          $lookup: {
            from: CodeData.collection.name,
            localField: 'value.cfs_pp_rcc_itemClassificationCode',
            foreignField: '_id',
            as: 'value.cfs_pp_rcc_itemClassificationCode',
          },
        },
      ])
      .sort({ 'value.cfs_tr_atp_lmt_payableAmount': -1 })
      .limit(10)
      .toArray()
    //  console.log(codeResult.value.)
    codeResultCodeData.forEach((item) => {
      topTen3.push({
        name: item.value.cfs_pp_rcc_itemClassificationCode[0].type,
        value: Math.round(item.value.cfs_tr_atp_lmt_taxExclusiveAmount),

        color: '#cccccc',
      })
    })

    res.status(200).send({
      top: { pre, pub, ev, adj, resu, anul },
      topTen,
      topTen2,
      mapsResult,
      topTen3,
    })
  } catch (e) {
    console.log(e)
    res.status(500).send(e)
  }
}
////////////////////
exports.getContratos = async (req, res, next) => {
  const { type, dateStart, dateEnd } = req.params
  // var start = new Date(
  //   dateStart.split('-')[0],
  //   dateStart.split('-')[1],
  //   dateStart.split('-')[2]
  // )
  // var end = new Date(
  //   dateEnd.split('-')[0],
  //   dateEnd.split('-')[1],
  //   dateEnd.split('-')[2]
  // )
  try {
    const listData = await Data.find({
      $and: [
        // { cfs_tp_tsdp_endDatetime: $lte(start) },
        // { cfs_tp_tsdp_endDatetime: $gte(end) },
        { cfs_contractFolderStatusCode: type.toUpperCase() },
      ],
    }).sort({ updated: -1 })
    res.status(200).send({ listData })
  } catch (e) {
    res.status(500).send(e)
  }
}
//////////////////////////
exports.getEmpresas = async (req, res, next) => {
  try {
    const connection = mongoose.connection
    const collection = await connection.db.collection('MapReduceName')
    const empresas = await collection
      .find()
      .sort({ 'value.cfs_tr_atp_lmt_payableAmount': -1 })
      .toArray()
    res.status(200).send({ empresas })
  } catch (e) {
    console.log(e)
    res.status(500).send(e)
  }
}

exports.getEmpresa = async (req, res, next) => {
  const { _id } = req.params
  try {
    const empresa = await Data.find({
      cfs_tr_wp_pi_iD: _id,
    }).sort({ updated: -1 })
    res.status(200).send({ empresa })
  } catch (e) {
    console.log(e)
    res.status(500).send(e)
  }
}
//////////////////////////
exports.getProvincias = async (req, res, next) => {
  try {
    const connection = mongoose.connection
    const collection = await connection.db.collection('MapReduceProvinces')
    const provincias = await collection
      .find()
      .sort({ 'value.cfs_tr_atp_lmt_payableAmount': -1 })
      .toArray()
    res.status(200).send({ provincias })
  } catch (e) {
    console.log(e)
    res.status(500).send(e)
  }
}

//////////////////////////FIN
exports.getAdjudicatarias = async (req, res, next) => {
  try {
    const connection = mongoose.connection
    const collection = await connection.db.collection('MapReduceName2')
    const adjudicatarias = await collection
      .find()
      .sort({ 'value.cfs_tr_atp_lmt_payableAmount': -1 })
      .toArray()
    res.status(200).send({ adjudicatarias })
  } catch (e) {
    console.log(e)
    res.status(500).send(e)
  }
}

exports.getAdjudicataria = async (req, res, next) => {
  try {
    const connection = mongoose.connection
    const collection = await connection.db.collection('MapReduceProvinces')
    const provincias = await collection
      .find()
      .sort({ 'value.cfs_tr_atp_lmt_payableAmount': -1 })
      .toArray()
    res.status(200).send({ provincias })
  } catch (e) {
    console.log(e)
    res.status(500).send(e)
  }
}
