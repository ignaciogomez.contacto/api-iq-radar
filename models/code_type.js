var mongoose = require('mongoose')

var Schema = mongoose.Schema

var dataSchema = new Schema({
  code: { type: Number },
  type: { type: String },
  type_en: { type: String },
})

module.exports = mongoose.model('CodeData', dataSchema)
