var mongoose = require('mongoose')

var Schema = mongoose.Schema

var dataSchema = new Schema(
  {
    id: { type: String, unique: true },
    title: { type: String },
    updated: { type: Date },
    codeprovince: { type: String, default: '' },
    cfs_contractFolderID: { type: String },
    cfs_contractFolderStatusCode: { type: String },

    cfs_lcp_ContractingPartyTypeCode: { type: Number },
    cfs_lcp_p_websiteURI: { type: String },
    cfs_lcp_p_partyIdentification: [{ ID: { type: String } }],
    cfs_lcp_p_pn_name: { type: String },
    cfs_lcp_p_pa_cityName: { type: String },
    cfs_lcp_p_pa_postalZone: { type: String },
    cfs_lcp_p_pa_al_line: { type: String },
    cfs_lcp_p_pa_c_identificationCode: { type: String },
    cfs_lcp_p_pa_c_name: { type: String },
    cfs_lcp_p_c_name: { type: String },
    cfs_lcp_p_c_telephone: { type: String },
    cfs_lcp_p_c_telefax: { type: String },
    cfs_lcp_p_c_electronicMail: { type: String },

    cfs_pp_name: { type: String },
    cfs_pp_typeCode: { type: String },
    //falta puesto
    cfs_pp_SubTypeCode: { type: Number },

    cfs_pp_ba_estimatedOverallContractAmount: { type: Number },
    cfs_pp_ba_totalAmount: { type: Number },
    cfs_pp_ba_taxExclusiveAmount: { type: Number },
    cfs_pp_rcc_itemClassificationCode: {
      type: [Schema.Types.ObjectId],
      ref: 'CodeData',
      default: [],
    },

    cfs_pp_rl_countrySubentity: { type: String },
    cfs_pp_rl_countrySubentityCode: { type: String },
    cfs_pp_rl_a_c_identificationCode: { type: String },
    cfs_pp_rl_a_c_name: { type: String },
    cfs_pp_pp_durationMeasure: { type: Number },

    cfs_tr_resultCode: { type: Number },
    cfs_tr_description: { type: String },
    cfs_tr_awardDate: { type: String },
    cfs_tr_receivedTenderQuantity: { type: Number },
    cfs_tr_lowerTenderAmount: { type: Number },
    cfs_tr_higherTenderAmount: { type: Number },
    cfs_tr_wp_pi_iD: { type: String },
    cfs_tr_wp_pn_Name: { type: String },
    cfs_tr_atp_lmt_taxExclusiveAmount: { type: Number, default: 0 },
    cfs_tr_atp_lmt_payableAmount: { type: Number, default: 0 },

    cfs_tt_requiredCurriculaIndicator: { type: Boolean },
    //falta
    cfs_tt_VariantConstraintIndicator: { type: Number, default: 0 },

    cfs_tt_pldr_iD: { type: String },
    cfs_tt_tqr_technicalEvaluationCriteria: [
      {
        EvaluationCriteriaTypeCode: { type: String },
        Description: { type: String },
      },
    ],
    cfs_tt_tqr_financialEvaluationCriteria: [
      {
        EvaluationCriteriaTypeCode: { type: String },
        Description: { type: String },
      },
    ],
    cfs_tt_tqr_str_requirementTypeCode: { type: Number },
    //NUmber hecho
    cfs_tt_ast_Rate: { type: Number },

    cfs_tt_tqr_at_awardingCriteria: [
      { Description: { type: String }, WeightNumeric: { type: Number } },
    ],

    cfs_tt_tqr_at_l_iD: { type: String },

    cfs_tp_procedureCode: { type: Number },
    cfs_tp_urgencyCode: { type: Number },
    cfs_tp_contractingSystemCode: { type: Number },
    cfs_tp_submissionMethodCode: { type: Number },
    cfs_tp_tsdp_endDate: { type: String },
    cfs_tp_tsdp_endTime: { type: String },
    cfs_tp_tsdp_endDatetime: { type: Date },
    //Falta
    cfs_tp_at_AuctionConstraintIndicator: { type: Number },
    cfs_ldr_iD: { type: String },
    cfs_ldr_a_er_uri: { type: String },
    cfs_ldr_a_er_documentHash: { type: String },
    cfs_tdr_iD: { type: String },
    cfs_tdr_a_er_uri: { type: String },
    cfs_tdr_a_er_documentHash: { type: String },

    cfs_tdr_iD: { type: String },
    cfs_tdr_a_e_uri: { type: String },
    cfs_tdr_a_e_documentHash: { type: String },
    cfs_AdditionalDocumentReference: [
      { ID: { type: String }, Attachment: { type: Object } },
    ],
    cfs_validNoticeInfo: { type: Array },
    cfs_gd_gddr_iD: { type: String },
    cfs_gd_gddr_a_er_uri: { type: String },
    cfs_gd_gddr_a_er_fileName: { type: String },

    //falta
    cfs_GeneralDocument: { type: Array },
  },
  { timestamps: true }
)

// var dataSchema = new Schema({
//   result: Object,
// })
module.exports = mongoose.model('Data', dataSchema)
