const express = require('express')
const router = express.Router()
const dataController = require('../controllers/data')
//var middleware = require('../middleware/identify')

router.get('/data', dataController.getData)

router.get('/contratos/:type', dataController.getContratos)

router.get('/empresas', dataController.getEmpresas)
router.get('/empresa/:_id', dataController.getEmpresa)

router.get('/adjudicatarias', dataController.getAdjudicatarias)
router.get('/adjudicataria/:_id', dataController.getAdjudicataria)

router.get('/provincias', dataController.getProvincias)

module.exports = router
